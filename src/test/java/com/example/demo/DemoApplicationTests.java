package com.example.demo;

import com.example.demo.dto.Album;
import com.example.demo.dto.Artist;
import com.example.demo.dto.Menu;
import com.example.demo.dto.Song;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
@RunWith(SpringRunner.class)
//@Transactional
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	DemoService demoService;


	@Test
	public void selectArtist() {
		List<Artist> artists = demoService.selectArtist();

		artists.forEach(obj -> {
			System.out.println(obj);
		});

		Assert.isTrue(Optional.ofNullable(artists).isPresent(), "Stock Null");
	}


	@Test
	public void selectArtistMap() {
		List<Artist> artists = demoService.selectArtistMap();

		artists.forEach(obj -> {
			System.out.println(obj);
		});

		Assert.isTrue(Optional.ofNullable(artists).isPresent(), "Stock Null");
	}

	@Test
	public void selectAlbumMap() {
		List<Album> albums = demoService.selectAlbumMap();

		albums.forEach(obj -> {
			System.out.println(obj);
		});

		Assert.isTrue(Optional.ofNullable(albums).isPresent(), "Stock Null");
	}

	@Test
	public void selectSongMap() {
		List<Song> songs = demoService.selectSongMap();

		songs.forEach(obj -> {
			System.out.println(obj);
		});

		Assert.isTrue(Optional.ofNullable(songs).isPresent(), "Stock Null");
	}

	@Test
	public void insertArtist() throws JsonProcessingException {
		Artist artist = Artist.builder()
//				.name(RandomStringUtils.randomAlphabetic(10))
				.name("666644")
				.creator(RandomStringUtils.randomAlphabetic(10))
				.build();

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		String str = objectMapper.writeValueAsString(artist);
		System.out.println(str);

		demoService.insertArtist(str);
	}

	@SneakyThrows
	@Test
	public void select() {

		Menu menu = demoService.selectMenu();

		List<Menu> list = new ArrayList<Menu>();

		recursive(menu, list);
		list.forEach(m -> {
			m.setChildren(null);
			m.setPoolId(NumberUtils.INTEGER_ONE);
		});

//		list.stream().map(Menu::getName).forEach(System.out::println);
//		list.stream().map(Menu::getThisUid).forEach(System.out::println);
//		list.stream().map(Menu::getParentUid).forEach(System.out::println);
//		list.stream().map(Menu::getId).forEach(System.out::println);
//		list.stream().map(Menu::getUpId).forEach(System.out::println);
//		list.stream().map(Menu::getLevel).forEach(System.out::println);

		ObjectMapper objectMapper = new ObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

		System.out.println(objectMapper.writeValueAsString(list));
		Assert.isTrue(Optional.ofNullable(menu).isPresent(), "is not null");
	}

	public void recursive(Menu menu, List<Menu> list) {

		menu.setId(new Random().ints().findFirst().getAsInt());
		list.add(menu);
		if(false == menu.getChildren().isEmpty()) {

			Iterator<Menu> itr = menu.getChildren().iterator();
			Integer upId = menu.getId();
			while(itr.hasNext()) {
				Menu m = itr.next();
				m.setUpId(upId);
				recursive(m, list);
			}
		}
	}



	@Test
	public void selectTree() {

		List<Menu> menu = demoService.selectTree();

		Assert.isTrue(Optional.ofNullable(menu).isPresent(), "is not null");
	}

}
