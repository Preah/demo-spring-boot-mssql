CREATE OR ALTER PROCEDURE [DBO].[NSP_PAGE_ARTISTS]
      @JSON NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    DECLARE @PAGE_NO   TINYINT = JSON_VALUE(@JSON, '$.page_no')
    DECLARE @PAGE_SIZE TINYINT = JSON_VALUE(@JSON, '$.page_size')
    declare @artist_id int = JSON_VALUE(@JSON, '$.artist_id')

    SELECT ROW_NUMBER() OVER(ORDER BY ARTIST_ID) AS 'row_num'
         , COUNT(*) OVER()                       AS 'total_count'
         , @PAGE_NO                              AS 'page_no'
         , @PAGE_SIZE                            AS 'page_size'
         , *
      FROM ARTISTS
     WHERE ARTIST_ID = ISNULL(, ARTIST_ID)
       AND NAME LIKE CONCAT(ISNULL(JSON_VALUE(@JSON, '$.name'), NAME), '%')
     ORDER BY ARTIST_ID DESC
    OFFSET (@PAGE_NO * @PAGE_SIZE) ROW FETCH NEXT @PAGE_SIZE ROW ONLY

END

EXEC NSP_PAGE_ARTISTS '{"page_no":1, "page_size": 3, "artist_id": 20}'