DECLARE @JSON NVARCHAR(MAX) = N'[
  {
    "id": -733183334,
    "name": "레이아웃",
    "up_id": null,
    "children": null,
    "pool_id": 1
  },
  {
    "id": -1322854526,
    "name": "백그라운드 컬러",
    "up_id": -733183334,
    "children": null,
    "pool_id": 1
  },
  {
    "id": 728766322,
    "name": "백그라운드 컬러 목록",
    "up_id": -1322854526,
    "children": null,
    "pool_id": 1
  },
  {
    "id": -1663156178,
    "name": "백그라운드 컬러 수정",
    "up_id": -1322854526,
    "children": null,
    "pool_id": 1
  },
  {
    "id": 1063477900,
    "name": "백그라운드 컬러 상세보기",
    "up_id": -1322854526,
    "children": null,
    "pool_id": 1
  },
  {
    "id": 138911069,
    "name": "백그라운드 컬러 등록",
    "up_id": -1322854526,
    "children": null,
    "pool_id": 1
  },
  {
    "id": -1733319072,
    "name": "백그라운드 이미지",
    "up_id": -733183334,
    "children": null,
    "pool_id": 1
  },
  {
    "id": -1324306737,
    "name": "백그라운드 이미지 목록",
    "up_id": -1733319072,
    "children": null,
    "pool_id": 1
  },
  {
    "id": 492955463,
    "name": "백그라운드 이미지 수정",
    "up_id": -1733319072,
    "children": null,
    "pool_id": 1
  },
  {
    "id": -1434177227,
    "name": "백그라운드 이미지 등록",
    "up_id": -1733319072,
    "children": null,
    "pool_id": 1
  }
]'

EXEC MENU_TREE @JSON;
