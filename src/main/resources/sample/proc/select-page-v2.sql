CREATE OR ALTER PROCEDURE [DBO].[NSP_PAGE_ARTISTS]
      @JSON NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT A.*
         , B.ALBUM_ID     AS 'album.album_id'
         , B.TITLE        AS 'album.title'
         , B.CREATOR      AS 'album.creator'
         , C.SONG_ID      AS 'album.song.song_id'
         , C.NAME         AS 'album.song.name'
         , C.CREATOR      AS 'album.song.creator'
    FROM (SELECT ROW_NUMBER() OVER(ORDER BY ARTIST_ID) AS 'row_num'
               , JSON_VALUE(@JSON, '$.page_no')        AS 'page_no'
               , JSON_VALUE(@JSON, '$.page_size')      AS 'page_size'
               , COUNT(*) OVER()                       AS 'total_count'
               , *
            FROM ARTISTS
           WHERE ARTIST_ID = ISNULL(JSON_VALUE(@JSON, '$.artist_id'), ARTIST_ID)
             AND NAME LIKE CONCAT(ISNULL(JSON_VALUE(@JSON, '$.name'), NAME), '%')
           ORDER BY ARTIST_ID DESC
          OFFSET (JSON_VALUE(@JSON, '$.page_no') * JSON_VALUE(@JSON, '$.page_size') ROW
           FETCH NEXT CONVERT(INT, JSON_VALUE(@JSON, '$.page_size')) ROW ONLY) A
    LEFT OUTER JOIN ALBUMS B ON A.ARTIST_ID = B.ARTIST_ID
    LEFT OUTER JOIN SONGS C  ON B.ALBUM_ID = C.ALBUM_ID
END

EXEC NSP_PAGE_ARTISTS '{"page_no":1, "page_size": 3, "artist_id": 20}'