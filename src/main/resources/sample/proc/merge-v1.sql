CREATE OR ALTER PROCEDURE [DBO].[NSP_MERGE_ARTIST]
      @JSON NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    BEGIN TRAN

        DECLARE @J_ARTISTS TABLE (
               ARTIST_ID   INT
             , NAME        NVARCHAR(MAX)
             , CREATOR     NVARCHAR(MAX)
        );

        INSERT INTO @J_ARTISTS
        SELECT *
        FROM OPENJSON(@JSON)
        WITH (
               ARTIST_ID   INT
             , NAME        NVARCHAR(MAX) '$.name'
             , CREATOR     NVARCHAR(MAX) '$.creator'
        );

        DELETE FROM SONGS   WHERE ALBUM_ID      IN (SELECT X.ALBUM_ID FROM ALBUMS X INNER JOIN @J_ARTISTS Z ON X.ARTIST_ID <> Z.ARTIST_ID AND Z.ARTIST_ID IS NOT NULL);
        DELETE FROM ALBUMS  WHERE ARTIST_ID NOT IN (SELECT ARTIST_ID FROM @J_ARTISTS WHERE ARTIST_ID IS NOT NULL);
        DELETE FROM ARTISTS WHERE ARTIST_ID NOT IN (SELECT ARTIST_ID FROM @J_ARTISTS WHERE ARTIST_ID IS NOT NULL);

        MERGE ARTISTS A
        USING @J_ARTISTS B
        ON A.ARTIST_ID = B.ARTIST_ID
        WHEN MATCHED THEN
            UPDATE SET A.NAME    = B.NAME
                     , A.CREATOR = B.CREATOR
        WHEN NOT MATCHED AND B.ARTIST_ID IS NULL THEN
            INSERT VALUES(B.NAME, B.CREATOR);

    COMMIT TRAN
END

-- UPDATE
EXEC NSP_MERGE_ARTIST '{"artist_id" : 1, "name": "merge update", "creator": "merge update"}'
EXEC NSP_MERGE_ARTIST '[{"artist_id" : 1, "name": "merge update 1"}, {"artist_id" : 2, "name": "merge update 2"}]'

-- INSERT
EXEC NSP_MERGE_ARTIST '{"name": "merge insert"}'
EXEC NSP_MERGE_ARTIST '[{"name": "merge insert 1"}, {"name": "merge insert 2"}, {"name": "merge insert 3"}]'