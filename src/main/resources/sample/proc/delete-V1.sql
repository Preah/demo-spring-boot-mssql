CREATE OR ALTER PROCEDURE [DBO].[NSP_DELETE_ARTIST]
      @JSON NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    BEGIN TRAN

        DELETE FROM ARTISTS
        WHERE ARTIST_ID = JSON_VALUE(@JSON, '$.artist_id');

    COMMIT TRAN
END

-- 단건 삭제
EXEC NSP_DELETE_ARTIST '{"artist_id": 1}'
