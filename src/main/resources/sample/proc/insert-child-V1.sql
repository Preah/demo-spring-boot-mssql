CREATE OR ALTER PROCEDURE [DBO].[NSP_INSERT_ARTIST_CHILD]
      @JSON NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    BEGIN TRAN

        -- ARTISTS
        INSERT INTO ARTISTS (NAME, CREATOR)
        VALUES(JSON_VALUE(@JSON, '$.name'), JSON_VALUE(@JSON, '$.creator'));

        DECLARE @ARTIST_ID INT = (SELECT @@IDENTITY);

        -- ALBUMS
        DECLARE @J_ALBUMS TABLE (
               TITLE       NVARCHAR(MAX)
             , CREATOR     NVARCHAR(MAX)
        );

        INSERT INTO @J_ALBUMS
        SELECT *
        FROM OPENJSON(@JSON, '$.artist.albums')
        WITH (
               TITLE       NVARCHAR(MAX) '$.title'
             , CREATOR     NVARCHAR(MAX) '$.creator'
        );

        INSERT INTO ALBUMS (
               ARTIST_ID
             , TITLE
             , CREATOR
             )
        SELECT @ARTIST_ID AS ARTIST_ID
             , TITLE
             , CREATOR
        FROM @J_ALBUMS;

    COMMIT TRAN
END


-- ARTIST : 1 / ALBUMS : 2
EXEC NSP_INSERT_ARTIST_CHILD '{"artist" : {"name": "insert 1", "albums": [{"title": "child 1"}, {"title": "child 2"}]}}'
