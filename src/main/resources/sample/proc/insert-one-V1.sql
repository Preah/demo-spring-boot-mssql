CREATE OR ALTER PROCEDURE [DBO].[NSP_INSERT_ARTIST] @JSON NVARCHAR(MAX)
AS
BEGIN
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    BEGIN TRAN

        INSERT INTO ARTISTS
        (
            NAME
          , CREATOR
        )
        VALUES(
                JSON_VALUE(@JSON, '$.name')
              , JSON_VALUE(@JSON, '$.creator')
        );

    COMMIT TRAN
END

EXEC NSP_INSERT_ARTIST '{"name": "00000000", "creator": "creator"}';