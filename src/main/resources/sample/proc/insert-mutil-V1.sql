CREATE OR ALTER PROCEDURE [DBO].[NSP_INSERT_ARTIST_MUTIL] @JSON NVARCHAR(MAX)
AS
BEGIN
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    BEGIN TRAN

        INSERT INTO ARTISTS ( NAME
                            , CREATOR)
        SELECT NAME
             , CREATOR
        FROM OPENJSON(@JSON)
        WITH (
              NAME    NVARCHAR(MAX) '$.name'
            , CREATOR NVARCHAR(MAX) '$.creator'
        );

    COMMIT TRAN

END
GO

EXEC NSP_INSERT_ARTIST_MUTIL '[{"name": "00000000", "creator": "creator"}, {"name": "00000000", "creator": "creator"}]';