CREATE OR ALTER PROCEDURE [DBO].[NSP_INSERT_TB_ARTIST_MULTI] @JSON NVARCHAR(MAX)
AS
BEGIN
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    BEGIN TRAN

        INSERT INTO TB_ARTISTS ( NAME
                              , CREATOR)
        SELECT NAME
             , CREATOR
        FROM OPENJSON(@JSON)
        WITH (
              NAME    NVARCHAR(MAX) '$.name'
            , CREATOR NVARCHAR(MAX) '$.creator'
        );

    COMMIT TRAN

END

EXEC NSP_INSERT_TB_ARTIST_MULTI '[{"name": "000000001", "creator": "creator1"}, {"name": "000000002", "creator": "creator2"}]';