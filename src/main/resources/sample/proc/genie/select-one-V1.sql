CREATE OR ALTER PROCEDURE [DBO].[NSP_SELECT_TB_ARTIST]
      @JSON NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT A.ARTIST_ID    AS 'artist_id'
         , A.NAME         AS 'name'
         , A.CREATOR      AS 'creator'
         , B.ALBUM_ID     AS 'album.album_id'
         , B.TITLE        AS 'album.title'
         , B.CREATOR      AS 'album.creator'
         , C.SONG_ID      AS 'album.song.song_id'
         , C.NAME         AS 'album.song.name'
         , C.CREATOR      AS 'album.song.creator'
    FROM TB_ARTISTS A
    LEFT OUTER JOIN ALBUMS B ON A.ARTIST_ID = B.ARTIST_ID
    LEFT OUTER JOIN SONGS  C ON B.ALBUM_ID  = C.ALBUM_ID
    WHERE A.ARTIST_ID = JSON_VALUE(@JSON, '$.artist_id')
END

EXEC NSP_SELECT_TB_ARTIST '{"artist_id": 20}'