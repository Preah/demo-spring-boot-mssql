CREATE OR ALTER PROCEDURE [DBO].[NSP_UPDATE_TB_ARTIST_MULTI]
      @JSON NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    BEGIN TRAN

        UPDATE TB_ARTISTS
        SET    NAME    = B.NAME
             , CREATOR = B.CREATOR
        FROM TB_ARTISTS A
        INNER JOIN (SELECT *
                      FROM OPENJSON(@JSON)
                      WITH (
                             ARTIST_ID   INT           '$.artist_id'
                           , NAME        NVARCHAR(MAX) '$.name'
                           , CREATOR     NVARCHAR(MAX) '$.creator'
                      )
        ) B
         ON A.ARTIST_ID = B.ARTIST_ID
        AND B.ARTIST_ID IS NOT NULL;

    COMMIT TRAN
END


EXEC NSP_UPDATE_ARTIST_MULTI '[{"artist_id" : 1, "name": "update to", "creator": "update"}, {"artist_id" : 2, "name": "update to", "creator": "update"}]'
