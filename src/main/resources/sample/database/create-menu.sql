-- Drop table

-- DROP TABLE model.dbo.menu GO

CREATE TABLE menu (
	id int IDENTITY(0,1) NOT NULL,
	up_id int NULL,
	pool_id int NULL,
	name nvarchar(100) COLLATE Korean_wansung_CI_AS NULL,
	CONSTRAINT menu_PK PRIMARY KEY (id),
	CONSTRAINT menu_FK FOREIGN KEY (up_id) REFERENCES menu(id)
)

INSERT INTO menu (up_id,name, pool_id) VALUES
(NULL,'레이아웃', 1)
,(0,'백그라운드 컬러', 1)
,(0,'백그라운드 이미지', 1)
,(1,'백그라운드 컬러 목록', 1)
,(1,'백그라운드 컬러 수정', 1)
,(1,'백그라운드 컬러 상세보기', 1)
,(2,'백그라운드 이미지 목록', 1)
,(2,'백그라운드 이미지 수정', 1)
,(2,'백그라운드 이미지 등록', 1)
,(1,'백그라운드 컬러 등록', 1)
;