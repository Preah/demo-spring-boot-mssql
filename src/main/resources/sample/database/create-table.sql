use model;


-- Drop table

-- DROP TABLE model.dbo.artists GO
--CREATE SEQUENCE model.dbo.seq_artist START WITH 1 INCREMENT BY 1;

CREATE TABLE model.dbo.artists (
	artist_id int IDENTITY(1,1) NOT NULL,
	name nvarchar(255),
	creator nvarchar(100),
	CONSTRAINT PK_artists PRIMARY KEY (artist_id)
);

-- Drop table

-- DROP TABLE model.dbo.albums GO
-- CREATE SEQUENCE model.dbo.seq_album START WITH 1 INCREMENT BY 1;

CREATE TABLE model.dbo.albums (
	album_id int IDENTITY(1,1) NOT NULL,
	artist_id int NOT NULL,
	title nvarchar(255),
	creator nvarchar(100),
	CONSTRAINT PK_albums PRIMARY KEY (album_id),
	CONSTRAINT FK_albums_artists FOREIGN KEY (artist_id) REFERENCES model.dbo.artists(artist_id) ON DELETE CASCADE ON UPDATE CASCADE
);


-- Drop table

-- DROP TABLE model.dbo.songs GO
-- CREATE SEQUENCE model.dbo.seq_song START WITH 1 INCREMENT BY 1;

CREATE TABLE model.dbo.songs (
	song_id int IDENTITY(1,1) NOT NULL,
	album_id int NOT NULL,
	name nvarchar(255),
	creator nvarchar(100),
	CONSTRAINT PK_songs PRIMARY KEY (song_id),
	CONSTRAINT FK_songs_albums FOREIGN KEY (album_id) REFERENCES model.dbo.albums(album_id) ON DELETE CASCADE ON UPDATE CASCADE
);