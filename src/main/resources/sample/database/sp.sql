CREATE PROCEDURE PRODUCTION.SP_STOCK
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    select top 1 * from production.stocks

SET NOCOUNT OFF
RETURN
END;


CREATE PROCEDURE PRODUCTION.SP_STOCKS
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    select * from production.stocks

SET NOCOUNT OFF
RETURN
END;


CREATE PROCEDURE PRODUCTION.SP_JOIN_ALL
AS
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    select a.store_id      as 'stock.store_id'
	     , a.product_id    as 'stock.product_id'
	     , a.quantity      as 'stock.quantity'
	     , b.product_name  as 'product.product_name'
	     , b.category_id   as 'product.category_id'
	     , b.model_year    as 'product.model_year'
	     , b.list_price    as 'product.list_price'
	from production.stocks a
	   , production.products b
	where a.product_id = b.product_id

SET NOCOUNT OFF
RETURN
END;



CREATE OR ALTER PROCEDURE [DBO].[NSP_PAGE_ALTISTS]
	@JSON NVARCHAR(MAX)
AS
BEGIN

	DECLARE @PAGE_NO    INT = 1
	DECLARE @PAGE_SIZE  INT = 10
	DECLARE @ARTIST_ID  INT = NULL
	DECLARE @NAME       NVARCHAR(MAX) = NULL

	SELECT @PAGE_NO    = PAGE_NO
	     , @PAGE_SIZE  = PAGE_SIZE
	     , @ARTIST_ID  = ARTIST_ID
	     , @NAME       = NAME
	FROM OPENJSON(@JSON)
	WITH (
	       PAGE_NO   INT
	     , PAGE_SIZE INT
	     , ARTIST_ID INT
	     , NAME      NVARCHAR(MAX)
	)

	SELECT A.*
         , B.ALBUM_ID     AS 'album.album_id'
         , B.TITLE        AS 'album.title'
         , B.CREATOR      AS 'album.creator'
         , C.SONG_ID      AS 'album.song.song_id'
         , C.NAME         AS 'album.song.name'
         , C.CREATOR      AS 'album.song.creator'
	FROM (SELECT ROW_NUMBER() OVER(ORDER BY ARTIST_ID) AS 'rownum'
	           , @PAGE_NO        AS 'page_no'
	           , @PAGE_SIZE      AS 'page_size'
	           , COUNT(*) OVER() AS 'totla_count'
	           , *
		    FROM ARTISTS
		   WHERE ARTIST_ID = ISNULL(@ARTIST_ID, ARTIST_ID)
		     AND NAME LIKE CONCAT(ISNULL(@NAME, NAME), '%')
	       ORDER BY ARTIST_ID DESC
	      OFFSET (@PAGE_NO-1) * @PAGE_SIZE ROW
	       FETCH NEXT @PAGE_SIZE ROW ONLY) A
    LEFT OUTER JOIN ALBUMS B ON A.ARTIST_ID = B.ARTIST_ID
    LEFT OUTER JOIN SONGS C ON B.ALBUM_ID = C.ALBUM_ID
END


EXEC nsp_page_altists '[{"PAGE_NO":1, "PAGE_SIZE": 3, "ARTIST_ID": 20}]'




/*
===============================================================================
오브젝트명	: NSP_INSERT_UPDATE_TB_IV_TV_ALBUM_CMS
오브젝트타입	: PROCEDURE
최초작성일	: 2020-02-06
최초작성자	: 박종윤
기능설명		: 지니CMS - 방송관리 > 공통 >앨범관리 > 등록/수정
실행예제		: EXEC DBO.NSP_INSERT_UPDATE_TB_IV_TV_ALBUM_CMS
수행서버		: GENIEDBC / ASP_MC
-------------------------------------------------------------------------------
추가작업내역 :

===============================================================================
*/
CREATE PROCEDURE [DBO].[NSP_INSERT_UPDATE_TB_IV_TV_ALBUM_CMS]
    @ALBUM_SEQ_ID			INT = 0,
    @ALBUM_ID				INT = 0,
    @PROGRAM_GUBUN          VARCHAR(20),
    @ALBUM_INFO				NVARCHAR(600),
    @SEASON_NUM				INT = 0,
    @VIEW_YN				CHAR(1),
    @DELETE_YN				CHAR(1),
    @RTN_CODE				INT = 0			OUTPUT,
    @RTN_MSG				VARCHAR(300)	OUTPUT
AS
BEGIN
	SET NOCOUNT ON

    MERGE	dbo.TB_IV_TV_ALBUM AS T
    USING	(SELECT @ALBUM_SEQ_ID, @ALBUM_ID, @PROGRAM_GUBUN, @ALBUM_INFO, @SEASON_NUM, @VIEW_YN, @DELETE_YN)
        AS S (ALBUM_SEQ_ID, ALBUM_ID, PROGRAM_GUBUN, ALBUM_INFO, SEASON_NUM, VIEW_YN, DELETE_YN)
    ON T.ALBUM_SEQ_ID = S.ALBUM_SEQ_ID
    WHEN	MATCHED THEN
        UPDATE SET	ALBUM_ID = S.ALBUM_ID,
                    ALBUM_INFO = S.ALBUM_INFO,
                    SEASON_NUM = S.SEASON_NUM,
                    VIEW_YN    = S.VIEW_YN
    WHEN	NOT MATCHED THEN
        INSERT (ALBUM_ID, PROGRAM_GUBUN, ALBUM_INFO, SEASON_NUM, DELETE_YN, VIEW_YN, REG_DATE)
        VALUES (S.ALBUM_ID, S.PROGRAM_GUBUN, S.ALBUM_INFO, S.SEASON_NUM, S.DELETE_YN, S.VIEW_YN, GETDATE());

    IF (@@ERROR <> 0) BEGIN GOTO ERRHANDLE END

    GOTO OKHANDLE

    ---===================================
    --	ERROR HANDLE AREA
    ---===================================
    OKHANDLE:
    SET @RTN_CODE = 0
    SET @RTN_MSG = '정상 처리'
    RETURN (0)

    ERRHANDLE:
    SET @RTN_CODE = 3
    SET @RTN_MSG = '오류 발생'
    RETURN (3)
END