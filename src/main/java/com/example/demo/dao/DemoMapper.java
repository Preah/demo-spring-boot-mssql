package com.example.demo.dao;

import com.example.demo.dto.Album;
import com.example.demo.dto.Artist;
import com.example.demo.dto.Menu;
import com.example.demo.dto.Song;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Mapper
public interface DemoMapper {

    public List<Artist> selectArtist();
    public List<Artist> selectArtistMap();
    public List<Album> selectAlbumMap();
    public List<Song> selectSongMap();
    public void insertArtist(String artist);
    public Menu selectMenu();
    public List<Menu> selectTree();
}
