package com.example.demo;

import com.example.demo.dao.DemoMapper;
import com.example.demo.dto.Album;
import com.example.demo.dto.Artist;
import com.example.demo.dto.Menu;
import com.example.demo.dto.Song;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

@Service
@Transactional
public class DemoService {

    private final DemoMapper demoMapper;

    public DemoService(DemoMapper demoMapper) {
        this.demoMapper = demoMapper;
    }

    public List<Artist> selectArtist() {
        return demoMapper.selectArtist();
    }
    public List<Artist> selectArtistMap() {
        return demoMapper.selectArtistMap();
    }
    public List<Album> selectAlbumMap() {
        return demoMapper.selectAlbumMap();
    }
    public List<Song> selectSongMap() {
        return demoMapper.selectSongMap();
    }

    public void insertArtist(String artist) { demoMapper.insertArtist(artist); }

    public Menu selectMenu() {
        return demoMapper.selectMenu();
    }
    public List<Menu> selectTree() {
        return demoMapper.selectTree();
    }
}

