package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class Menu {

    private Integer id;
    private String name;

    private Integer upId;
    List<Menu> children;

//    private String thisUid;
//    private String parentUid;
    private Integer poolId;
}
