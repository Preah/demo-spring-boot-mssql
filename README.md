```plantuml
@startuml

database MSSQL {

    folder PARENT {
    
        [PARENT3]
        [PARENT2]
        [PARENT1]
    }
    

    folder CHILD {
    
        [CHILD3]
        [CHILD2]
        [CHILD1]
        [PARENT1]
    }
    
}


node "MyBatis" {

    [Select A.*, B.* \nFrom PARENT A, CHILD B]
    [Result]
    [Select A.*, B.* \nFrom PARENT A, CHILD B] --> [Result]
}

[Select A.*, B.* \nFrom PARENT A, CHILD B] --> [PARENT1]
[Select A.*, B.* \nFrom PARENT A, CHILD B] --> [PARENT2]
[Select A.*, B.* \nFrom PARENT A, CHILD B] --> [PARENT3]
[Select A.*, B.* \nFrom PARENT A, CHILD B] --> [CHILD1]
[Select A.*, B.* \nFrom PARENT A, CHILD B] --> [CHILD2]
[Select A.*, B.* \nFrom PARENT A, CHILD B] --> [CHILD3]

node "Java" {
   [ParentVO]
   [ChildVO]
   [Result] --> [ParentVO]
   [Result] --> [ChildVO]
}


@enduml
```